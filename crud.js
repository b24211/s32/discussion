let http = require("http");

// Mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "John",
		"email": "john@mail.com"
	}
];

http.createServer(function(request, response){

	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}

	// POST request
	if(request.url == "/users" && request.method == "POST"){
		let requestBody = '';

		request.on('data', function (data){
			requestBody += data;
		})

		request.on('end', function(){
			console.log(typeof requestBody);

			requestBody = JSON.parse(requestBody);

			let newUser = {
			"name": requestBody.name,
			"email": requestBody.email
			}

			// Add a new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})

	}
}).listen(4000);

console.log('Server is running at localhost:4000');